package common

import (
	"github.com/go-redis/redis/v7"
	"time"
)

var red *redis.Client

func init() {
	red = redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
		DB:   1,
	})
}
func SetCode(key string, code int) bool {
	_, err := red.Set(key, code, time.Second*120).Result()
	if err != nil {
		return false
	}
	return true
}

func GetReidsCode(key string) string {
	r, _ := red.Get(key).Result()
	return r
}
