package common

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
)

func Auth(name, idcard string) map[string]interface{} {
	client := &http.Client{}
	body := "name=" + name + "&idcard=" + idcard
	req, _ := http.NewRequest("POST", "http://eid.shumaidata.com/eid/check", strings.NewReader(body))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", "APPCODE d6424ffc003142fb951e5231a98c04b9")
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	b, _ := io.ReadAll(resp.Body)
	var r map[string]interface{}
	json.Unmarshal(b, &r)
	return r
}
