package common

import (
	"github.com/beego/beego/v2/client/orm"
	_ "github.com/go-sql-driver/mysql"
)

type Users struct {
	Id       int
	Mobile   string
	Password string
	Name     string
	Idcard   string
	Status   int
}

func init() {
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", "root:root@tcp(127.0.0.1:3306)/2107a")
	orm.RegisterModel(new(Users))
	orm.RunSyncdb("default", false, true)
	orm.Debug = true
}
