package svc

import (
	"github.com/zeromicro/go-zero/zrpc"
	"zg2b/10.19/api/internal/config"
	"zg2b/10.19/rpc/usersclient"
)

type ServiceContext struct {
	Config   config.Config
	UsersRpc usersclient.Users
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:   c,
		UsersRpc: usersclient.NewUsers(zrpc.MustNewClient(c.UsersRpc)),
	}
}
