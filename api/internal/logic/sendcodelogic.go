package logic

import (
	"context"
	"zg2b/10.19/rpc/usersclient"

	"zg2b/10.19/api/internal/svc"
	"zg2b/10.19/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type SendCodeLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewSendCodeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SendCodeLogic {
	return &SendCodeLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *SendCodeLogic) SendCode(req *types.SendRequest) (resp *types.SendResponse, err error) {
	// todo: add your logic here and delete this line
	d, _ := l.svcCtx.UsersRpc.Sendcode(l.ctx, &usersclient.SendRequest{
		Mobile: req.Mobile,
	})
	return &types.SendResponse{
		Code:    d.Code,
		Message: d.Message,
	}, nil
}
