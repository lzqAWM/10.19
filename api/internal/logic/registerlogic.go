package logic

import (
	"context"
	"github.com/dgrijalva/jwt-go/v4"
	"time"
	"zg2b/10.19/common"
	"zg2b/10.19/rpc/usersclient"

	"zg2b/10.19/api/internal/svc"
	"zg2b/10.19/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type RegisterLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewRegisterLogic(ctx context.Context, svcCtx *svc.ServiceContext) *RegisterLogic {
	return &RegisterLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *RegisterLogic) Register(req *types.ReRequest) (resp *types.ReResponse, err error) {
	// todo: add your logic here and delete this line
	d, _ := l.svcCtx.UsersRpc.Register(l.ctx, &usersclient.ReRequest{
		Mobile:   req.Mobile,
		Password: req.Password,
		Code:     req.Code,
	})
	if d.Code != 200 {
		return &types.ReResponse{
			Code:    d.Code,
			Message: d.Message,
		}, nil
	}
	token := common.CreateToken(&common.MyCustomClaims{
		Mobile: req.Mobile,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: jwt.NewTime(float64(l.svcCtx.Config.Auth.AccessExpire * time.Now().Unix())),
		},
	}, []byte(l.svcCtx.Config.Auth.AccessSecret))
	return &types.ReResponse{
		Code:    d.Code,
		Message: d.Message + " " + token,
	}, nil
}
