package logic

import (
	"context"
	"zg2b/10.19/rpc/usersclient"

	"zg2b/10.19/api/internal/svc"
	"zg2b/10.19/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type AuthLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewAuthLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AuthLogic {
	return &AuthLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *AuthLogic) Auth(req *types.AuRequest) (resp *types.AuResponse, err error) {
	// todo: add your logic here and delete this line
	d, _ := l.svcCtx.UsersRpc.Auth(l.ctx, &usersclient.AuRequest{
		Mobile: req.Mobile,
		Name:   req.Name,
		Idcard: req.Idcard,
	})
	return &types.AuResponse{
		Code:    d.Code,
		Message: d.Message,
	}, nil
}
