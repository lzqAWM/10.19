package logic

import (
	"context"
	"strconv"
	"zg2b/10.19/rpc/usersclient"

	"zg2b/10.19/api/internal/svc"
	"zg2b/10.19/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type UserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UserLogic {
	return &UserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *UserLogic) User(req *types.UsRequest) (resp *types.UsResponse, err error) {
	// todo: add your logic here and delete this line
	d, _ := l.svcCtx.UsersRpc.User(l.ctx, &usersclient.UsRequest{
		Mobile: req.Mobile,
	})
	return &types.UsResponse{
		Code:    d.Code,
		Message: d.Message,
		Data: map[string]string{
			"Id":       strconv.FormatInt(d.List.Id, 10),
			"Mobile":   d.List.Mobile,
			"Password": d.List.Password,
			"Name":     d.List.Name,
			"Idcard":   d.List.Idcard,
		},
	}, nil
}
