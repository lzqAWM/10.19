package logic

import (
	"context"
	"strconv"
	"zg2b/10.19/common"

	"zg2b/10.19/rpc/internal/svc"
	"zg2b/10.19/rpc/pb/users"

	"github.com/zeromicro/go-zero/core/logx"
)

type SendcodeLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSendcodeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SendcodeLogic {
	return &SendcodeLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SendcodeLogic) Sendcode(in *users.SendRequest) (*users.SendResponse, error) {
	// todo: add your logic here and delete this line
	mobile := in.Mobile
	code := common.GetCode(4)
	if !common.SendCode(mobile, code) {
		return &users.SendResponse{
			Code:    -1,
			Message: "发送失败",
		}, nil
	}
	CodeKey := "Users:Code_" + mobile
	common.SetCode(CodeKey, code)
	return &users.SendResponse{
		Code:    200,
		Message: "发送成功" + strconv.Itoa(code),
	}, nil
}
