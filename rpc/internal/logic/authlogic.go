package logic

import (
	"context"
	"fmt"
	"github.com/beego/beego/v2/client/orm"
	"zg2b/10.19/common"

	"zg2b/10.19/rpc/internal/svc"
	"zg2b/10.19/rpc/pb/users"

	"github.com/zeromicro/go-zero/core/logx"
)

type AuthLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAuthLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AuthLogic {
	return &AuthLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AuthLogic) Auth(in *users.AuRequest) (*users.AuResponse, error) {
	// todo: add your logic here and delete this line
	mobile := in.Mobile
	name := in.Name
	idcard := in.Idcard
	o := orm.NewOrm()
	var u common.Users
	err := o.QueryTable("users").Filter("mobile", mobile).One(&u)
	if err != nil {
		return &users.AuResponse{
			Code:    -1,
			Message: "用户不存在",
		}, nil
	}
	r := common.Auth(name, idcard)
	//if r["code"] != "0" {
	//	return &users.AuResponse{
	//		Code:    -1,
	//		Message: "无信息",
	//	}, nil
	//}
	fmt.Println(r)
	u.Name = name
	u.Idcard = idcard
	u.Status = 1
	o.Update(&u)
	return &users.AuResponse{
		Code:    200,
		Message: "认证成功",
	}, nil
}
