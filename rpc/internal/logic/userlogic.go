package logic

import (
	"context"
	"github.com/beego/beego/v2/client/orm"
	"zg2b/10.19/common"
	"zg2b/10.19/rpc/usersclient"

	"zg2b/10.19/rpc/internal/svc"
	"zg2b/10.19/rpc/pb/users"

	"github.com/zeromicro/go-zero/core/logx"
)

type UserLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UserLogic {
	return &UserLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UserLogic) User(in *users.UsRequest) (*users.UsResponse, error) {
	// todo: add your logic here and delete this line
	mobile := in.Mobile
	o := orm.NewOrm()
	var u common.Users
	err := o.QueryTable("users").Filter("mobile", mobile).One(&u)
	if err != nil {
		return &users.UsResponse{
			Code:    -1,
			Message: "用户不存在",
		}, nil
	}
	return &users.UsResponse{
		Code:    200,
		Message: "信息",
		List: &usersclient.Data{
			Id:       int64(u.Id),
			Mobile:   u.Mobile,
			Password: u.Password,
			Name:     u.Name,
			Idcard:   u.Idcard,
		},
	}, nil
}
