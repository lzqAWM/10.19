package logic

import (
	"context"
	"github.com/beego/beego/v2/client/orm"
	"strconv"
	"zg2b/10.19/common"

	"zg2b/10.19/rpc/internal/svc"
	"zg2b/10.19/rpc/pb/users"

	"github.com/zeromicro/go-zero/core/logx"
)

type RegisterLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewRegisterLogic(ctx context.Context, svcCtx *svc.ServiceContext) *RegisterLogic {
	return &RegisterLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *RegisterLogic) Register(in *users.ReRequest) (*users.ReResponse, error) {
	// todo: add your logic here and delete this line
	mobile := in.Mobile
	password := in.Password
	code1 := in.Code
	o := orm.NewOrm()
	var u common.Users
	err := o.QueryTable("users").Filter("mobile", mobile).One(&u)
	if err == nil {
		return &users.ReResponse{
			Code:    -1,
			Message: "用户已存在",
		}, nil
	}

	code2 := strconv.Itoa(int(code1))
	CodeKey := "Users:Code_" + mobile
	code := common.GetReidsCode(CodeKey)
	if code2 != code {
		return &users.ReResponse{
			Code:    -1,
			Message: "验证码错误",
		}, nil
	}
	u.Mobile = mobile
	u.Password = password
	o.Insert(&u)

	o.QueryTable("users").Filter("mobile", mobile).One(&u)
	if password != u.Password {
		return &users.ReResponse{
			Code:    -1,
			Message: "密码错误",
		}, nil
	}
	return &users.ReResponse{
		Code:    200,
		Message: "登录成功",
	}, nil
}
